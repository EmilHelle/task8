﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task8
{
    public abstract class Animal
    {
        private string family;
        private string diet;

        protected Animal(string family, string diet)
        {
            this.family = family;
            this.diet = diet;
        }

        
        public string Diet { get => diet; set => diet = value; }
        public string Family { get => family; set => family = value; }

        public abstract void MakeSound();
    }
}
