﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task8
{
    public class Cat : Animal
    {
        private string name;
        public Cat(string name,string family, string diet):base(family, diet)
        {
            this.name = name;
        }

        public string Name { get => name; set => name = value; }

        public override void MakeSound()
        {
            Console.WriteLine("Meow!");
        }
    }
}
