﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task8
{
    class Dog : Animal
    {
        private string name;
        public Dog(string name, string family, string diet) : base(family, diet)
        {
            this.name = name;
        }

        public string Name { get => name; set => name = value; }

        public override void MakeSound()
        {
            Console.WriteLine("Bark!");
        }
    }
}
