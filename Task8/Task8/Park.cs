﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task8
{
    class Park
    {
        private List<Animal> animals;

        public Park()
        {
            this.animals = new List<Animal>();
        }

        public Park(List<Animal> animals)
        {
            this.animals = animals;
        }

        public List<Animal> Animals { get => animals; set => animals = value; }

        public void Receive(Animal animal)
        {
            Animals.Add(animal);
        }

        public void Receive(List<Animal> animals)
        {
            Animals.AddRange(animals);
        }
    }
}
