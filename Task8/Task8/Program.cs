﻿using System;
using System.Collections.Generic;

namespace Task8
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to the Animal Park");

            //create animals
            Animal dog = new Dog("Doffen", "Canidae", "Carnivore");
            //list of felidae animals to be added to overloaded park method
            List<Animal> felidaeAnimals = new List<Animal> {
                 new Cat("Jojo", "Felidae", "Carnivore"),
                 //overloaded constructor
                 new Tiger("FierceTiger", "Orange", "Felidae", "Carnivore")
        };

            //park receives animals
            Park animalPark = new Park();
            animalPark.Receive(dog);
            animalPark.Receive(felidaeAnimals);

            //display behavior of animals in park
            Console.WriteLine("The animals make these sounds:");
            foreach(Animal animal in animalPark.Animals)
            {
                animal.MakeSound();
            }
        }
    }
}
