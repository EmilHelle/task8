﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task8
{
    class Tiger : Animal
    {
        private string name;
        private string color;
        public Tiger(string name, string family, string diet) : base(family, diet)
        {
            this.name = name;
        }

        public Tiger(string name, string color, string family, string diet) : base(family, diet)
        {
            this.name = name;
            this.color = color;
        }

        public override void MakeSound()
        {
            Console.WriteLine("Roar!");
        }
    }
}
